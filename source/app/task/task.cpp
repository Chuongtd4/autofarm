/*
 ============================================================================
 Name        : task.cpp
 Author      : chuongtd4
 Date        : 03/06/2019
 Version     :
 Copyright   : Your copyright notice
 Description : task C, Ansi-style
 ============================================================================
 */
#include <unistd.h>

#include "ak.h"

#include "sys_dbg.h"

#include "app.h"
#include "app_dbg.h"
#include "app_data.h"

#include "task_list.h"

#include "task.h"

#include "wifi.h"
#include "click.h"

q_msg_t task_1_mailbox;
q_msg_t task_2_mailbox;

void* task_1(void*) {
    wait_all_tasks_started();
    SYS_PRINT("task_1 run\n");
    timer_set(TASK_1, TASK1_OPENLINK, 100, TIMER_ONE_SHOT);
    ak_msg_t* msg;
    while(1)
    {
        msg = ak_msg_rev(TASK_1);

        switch (msg->header->sig) {
        case TASK1_OPENLINK: {
            SYS_PRINT("TASK1_OPENLINK\n");
            clickOpenLink();
            timer_set(TASK_1, TASK1_SKIP_AD, 20000, TIMER_ONE_SHOT);

        }
            break;
        case TASK1_SKIP_AD: {
            SYS_PRINT("TASK1_SKIP_AD\n");

            clickSkipAd();
            timer_set(TASK_1, TASK1_CLOSE_TAB, 3000, TIMER_ONE_SHOT);


        }
            break;

        case TASK1_CLOSE_TAB: {
            SYS_PRINT("TASK1_CLOSE_TAB\n");

            clickCloseTable();
            timer_set(TASK_1, TASK1_OPENLINK, 100, TIMER_ONE_SHOT);

        }
            break;
        default:
            SYS_PRINT("task_1 default\n");
            break;
        }

        /* free message */
        ak_msg_free(msg);
    }
    return (void*)0;
}

void* task_2(void*) {
    wait_all_tasks_started();
    SYS_PRINT("task_2 run\n");

    ak_msg_t* msg;
    while(1)
    {
        msg = ak_msg_rev(TASK_2);

        switch (msg->header->sig) {
        case SIG_1: {
            SYS_PRINT("task2_ SIG_1\n");
            timer_set(TASK_1, SIG_1, 2000, TIMER_PERIODIC);
        }
            break;
        case SIG_2: {
            SYS_PRINT("task_2 SIG_2\n");
        }
            break;
        default:
            SYS_PRINT("task_2 default\n");
            break;
        }
        /* free message */
        ak_msg_free(msg);
    }
    return (void*)0;
}


