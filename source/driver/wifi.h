#ifndef WIFI_H
#define WIFI_H

#ifdef __cplusplus
extern "C" {
#endif

#define DISCONNECT 1
#define CONNECT 0
int wifiStatus();
int wifiConnect();
int command(char* command_line);


#ifdef __cplusplus
 }
#endif

#endif // WIFI_H
