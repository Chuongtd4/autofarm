#ifndef TIME_H
#define TIME_H
#include <stdlib.h>
#include <time.h>
#include <stdio.h>'
#include <stdint.h>
#include <stdbool.h>
struct __timer
{   bool flag;
    uint32_t start;
    uint32_t now ;
    uint32_t wait;
};

void reset_timer(struct __timer *_timer);
void update_timer(struct __timer *_timer);
#endif // TIME_H
