#ifndef CLICK_H
#define CLICK_H
#ifdef __cplusplus
extern "C" {
#endif

int click();
int clickOpenLink();
int clickSkipAd();
int clickCloseTable();
#ifdef __cplusplus
}
#endif

#endif // CLICK_H
