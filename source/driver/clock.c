#include "clock.h"


void reset_timer(struct __timer *_timer){

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    _timer->now=0;
    _timer->start=tm.tm_sec;
    _timer->flag=0;
    _timer->wait=0;
}
void update_timer(struct __timer *_timer)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);

    if(_timer->flag==0){
        _timer->start=tm.tm_sec;
        _timer->flag=1;
    }
    else
        _timer->now =tm.tm_sec - _timer->start;
}
